set vb t_vb=
set backspace=indent,eol,start
set incsearch
set smartcase
set ignorecase
set hlsearch
set nowrapscan
set nowrap
set scrolloff=5
set sidescroll=1
set sidescrolloff=10
set tabstop=4
set shiftwidth=4
set wildmode=longest:list
set background=dark
set spelllang=en_us
set foldminlines=2
set foldlevelstart=1
set foldnestmax=2
set foldmethod=syntax
set modeline
set modelines=3
set keywordprg=man\ -s
set mouse=
set linebreak
set formatoptions-=o
set ruler
set laststatus=1
set nojoinspaces
set fileformats=unix,dos,mac
set t_Co=256
set completeopt=menuone
set tags=tags;
set backupdir=$HOME/.vim_swap//,/tmp
set directory=$HOME/.vim_swap//,/tmp
set guicursor=

map <ESC>u :nohlsearch<CR>
map j gj
map k gk
map Q ZQ
map H za
map \l :w<CR>:make<CR>
map - $
map _ ct_
map <C-n> gt
map <C-p> gT
map <C-h> :AS<CR>
nmap <Tab> :set expandtab!<CR>:set expandtab?<CR>
nmap <Insert> :set paste!<CR>:set paste?<CR>
nmap <C-j> :lnext<cr>
nmap <C-k> :lprevious<cr>
vmap P "_dP
inoremap <C-d> <C-t>
inoremap <C-a> <C-d>

let g:mundo_prefer_python3 = 1
nnoremap U :MundoToggle<CR>

let g:syntastic_python_checkers = ['pyflakes']
let g:syntastic_always_populate_loc_list = 1
let g:syntastic_auto_loc_list = 1
let g:syntastic_check_on_open = 0
let g:syntastic_check_on_wq = 0
let g:syntastic_mode_map = {'mode': 'passive', 'active_filetypes': [], 'passive_filetypes': []}
let g:syntastic_enable_signs = 0
nmap <F11> :SyntasticCheck<cr>
nmap <F12> :SyntasticReset<cr>

let g:ctrlp_map = '<c-o>'
let g:ctrlp_match_func = {'match': 'pymatcher#PyMatch'}

let g:rust_recommended_style = 0
let g:rust_fold = 1

filetype plugin indent on
syntax on
highlight Folded ctermbg=black
highlight Todo ctermbg=none ctermfg=lightcyan
" for ft=gitcommit
highlight DiffRemoved ctermfg=red cterm=bold
highlight DiffAdded ctermfg=green cterm=bold
" for vimdiff
highlight DiffAdd ctermfg=black ctermbg=green
highlight DiffDelete ctermfg=black ctermbg=red
highlight DiffChange ctermbg=darkgray
highlight DiffText ctermfg=black ctermbg=yellow

au BufNewFile,BufRead *.frag,*.vert,*.fp,*.vp,*.glsl setf glsl
au BufNewFile,BufRead *.jinja2 set ft=htmldjango
au BufNewFile,BufRead *.ccss set ft=clevercss
au BufNewFile,BufRead *.pp set ft=puppet foldmethod=indent

autocmd FileType c,cpp,perl,php,java,glsl set cindent foldmethod=syntax
autocmd FileType python set autoindent foldmethod=indent
autocmd FileType ruby set foldmethod=indent foldnestmax=4
autocmd FileType sh set autoindent foldmethod=syntax
autocmd FileType go set foldmethod=syntax foldlevel=0 foldnestmax=1
autocmd FileType javascript,less,html set foldnestmax=3 formatoptions-=o
autocmd FileType javascript,less set foldmethod=indent
autocmd FileType tex,latex set wrap showbreak=+ spell makeprg=pdflatex\ -interaction=nonstopmode\ %
autocmd FileType tf set foldmethod=indent foldlevel=0
autocmd FileType html,htmldjango,sml set autoindent
autocmd FileType man set nomod nolist readonly
autocmd FileType man map q :q<CR>
autocmd FileType diff map H zA
autocmd FileType diff map L za
autocmd FileType bzl set foldmethod=indent foldnestmax=3 foldlevel=2
autocmd FileType proto set foldmethod=syntax
autocmd FileType lua set foldmethod=indent foldnestmax=5 foldlevel=0

autocmd InsertEnter * if !exists('w:last_fdm') | let w:last_fdm=&foldmethod | setlocal foldmethod=manual | endif
autocmd InsertLeave,WinLeave * if exists('w:last_fdm') | let &l:foldmethod=w:last_fdm | unlet w:last_fdm | endif
