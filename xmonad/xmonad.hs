-- vim: noet:
import XMonad
import XMonad.Config.Gnome
import XMonad.Layout.NoBorders
import XMonad.Layout.ResizableTile
import XMonad.Layout.Tabbed
import XMonad.Layout.ThreeColumns
import XMonad.Hooks.ManageDocks
import XMonad.Hooks.ManageHelpers
import XMonad.Hooks.EwmhDesktops
import XMonad.Hooks.SetWMName
import XMonad.Actions.CycleWS     --for toggleWS
import XMonad.Util.EZConfig
import Data.List (isPrefixOf)
import qualified XMonad.StackSet as W

myManageHook :: ManageHook
myManageHook = composeAll [
	className =? "Firefox"    --> doShift "1",
	className =? "Iceweasel"  --> doShift "1",
	className =? "Chromium-browser" --> doShift "1",
	className =? "Slack"      --> doShift "0",
	className =? "discord"    --> doShift "9",
	className =? "Xfce4-notifyd" --> doF W.focusDown,
	className =? "MPlayer"    --> doFloat,
	className =? "mplayer2"   --> doFloat,
	className =? "Wine"       --> doFloat,
	className =? "graphics-console"--> doFloat,
	className =? "kvm"        --> doFloat,
	className =? "Gimp-2.6"   --> doFloat,
	className =? "Gimp"       --> doFloat,
	className =? "Stepmania"  --> doFloat,
	className =? "Plasma"     --> doFloat,
	className =? "Xfrun4"     --> doFloat,
	className =? "Wrapper"    --> doFloat,
	className =? "gource"     --> doFloat,
	className =? "Mate-panel" --> doFloat,
	isFullscreen              --> doFullFloat,
	manageDocks
	]

myLayout = avoidStruts $ smartBorders $ tiled ||| Mirror tiled ||| simpleTabbedAlways
	where
		tcol    = ThreeColMid nmaster delta 0.4
		tiled   = ResizableTall nmaster delta ratio []
		nmaster = 1
		delta   = 3 / 100
		--ratio   = toRational (2/(1 + sqrt 5 :: Double))
		ratio   = 0.5

main :: IO ()
main = xmonad $ gnomeConfig {
	workspaces                = ["1","2","3","4","5","6","7","8","9","0"],
	terminal                  = "mate-terminal --hide-menubar",
	manageHook                = manageHook gnomeConfig <+> myManageHook,
	layoutHook                = myLayout,
	logHook                   = ewmhDesktopsLogHook >> setWMName "LG3D",
	XMonad.borderWidth        = 2,
	XMonad.normalBorderColor  = "gray",
	XMonad.focusedBorderColor = "blue",
	focusFollowsMouse         = False
}
	`additionalKeysP` [
		("M-<Tab>",         toggleWS),
		--("M-`",             shiftTo Next EmptyWS),
		("M-0",             windows $ W.greedyView "0"),
		("M-S-0",           windows $ W.shift "0"),
		("<Print>",         spawn "scrot ~/screenshot.png -e bin/uplss"),
		("M-<Print>",       spawn "scrot ~/screenshot.png -s -e bin/uplss"),
		("S-<Print>",       spawn "scrot ~/screenshot.png"),
		("M-S-<Print>",     spawn "scrot ~/screenshot.png -s"),
		("M-S-t",           withFocused $ windows . W.sink),
		("M-S-q",           spawn "mate-session-save --logout-dialog"),
		("<XF86AudioPlay>", spawn "mocp -G"), -- XF86AudioPlay (pause)
		("<XF86AudioNext>", spawn "mocp -f"), -- XF86AudioNext
		("<XF86AudioPrev>", spawn "mocp -r"), -- XF86AudioPrev
		("<XF86AudioStop>", spawn "mocp -s")  -- XF86AudioStop
	]
	`removeKeysP` [
		"M-e",
		"M-r",
		"M-b",
		"M-t"
	]
